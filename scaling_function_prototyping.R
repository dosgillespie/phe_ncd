
# The aim of this code is to prototype a function 
# that takes specified input parameters
# and processes them to produce a table of scaling parameters 
# for use in adjusting parameters governing BMI values


# This script contains code taken from the SPHERE model that would allow the function's interface with the model to be tested 
# but this testing has not yet been done
# the initial focus is to develop the specification of the function


### SPHERE model setup code
# this code was intended to be used to allow the relevant SPHERE model code to be run
# to help understand the code and calculation context into which this function would fit

# Note: source common/scripts/define objects.R

# Load user inputs
source("user-inputs.R")

# Create path to common directory
common_dir <- file.path(getwd(), "common")

# Load global variables into environment - these include paths to data sources,

# servers and user credentials
source(file.path(common_dir, "scripts", "load-global-variables.R"), local = TRUE)

# Create path to PHE data lake directory
datalake_dir <- switch(
  
  tbl_source,
  
  "REMOTE" = file.path(
    
    gsub("/", .Platform$file.sep, global_vars[["DataLake_server"]]),
    
    global_vars[["LiveProject_db"]]
    
  ),
  
  "LOCAL" = file.path(
    
    gsub("/", .Platform$file.sep, global_vars[["DataLake_server"]]),
    
    global_vars[["LiveProject_db"]]
    
  ),
  
  "DISK" = file.path(getwd(), "DataLake")
  
)

# Specify SQL database name

if (tbl_source != "DISK" && dev_mode) {
  
  project_nm <- global_vars[["DevProject_db"]]
  
} else if (tbl_source != "DISK" && !dev_mode) {
  
  project_nm <- global_vars[["LiveProject_db"]]
  
} else {
  
  project_nm <- "SPHERE"
  
}

# Create/update/load lookup tables ---------------------------------------------
source(file.path(common_dir, "scripts", "prepare-lookups.R"), local = new.env())

src_ <- mdldata_src

scenario_ <- "BMI Increase Test"
para_ <- "bmi_mean"
simtime_ <- as.Date("2020-01-01")

#scaling_val <- source_tbl("scenario_scale", src_)

scaling_val <- source_tbl("scenario_scale", src_) %>%
  
  # Filter by scenario
  dplyr::filter(Scenario == scenario_) %>%
  
  # Filter by parameter name
  dplyr::filter(Parameter == para_) %>%
  
  # Filter by simulation year
  dplyr::filter(is.na(Year) | Year == lubridate::year(simtime_)) %>%
  
  # Filter by attribute values
  dplyr::filter(
    is.na(Age) | Age == 34,
    is.na(Sex) | Sex == 1
    # Add additional filters here
  ) %>%
  dplyr::select(Scaling) %>%
  dplyr::pull()

# Extract BMI distribution parameters ------------------------------------

# Pull projected BMI mean and standard deviation

trend_data <- source_tbl("trend_BMI", src_) %>%
  
  dplyr::filter(
    
    Sex == 1, AgeGrp == 7, Year == 2020
    
  )

mu <- trend_data %>%
  
  dplyr::select(Mean) %>%
  
  dplyr::pull()

sig <- trend_data %>%
  
  dplyr::select(StdDev) %>%
  
  dplyr::pull()

# Scale parameters
mean_scaling <- scaling_val

mu <- mu * mean_scaling

#sig <- sig * sd_scaling

sig <- 5.7

# Calculate BMI value ----------------------------------------------------

# Convert adjusted parameters to log-normal parameters

paras <- calculate_distparameters(mu, sig, "lognormal")

# Calculate BMI value

current_percentile <- .5

this_bmi <- qlnorm(
  current_percentile,
  meanlog = purrr::pluck(paras, 1), sdlog = purrr::pluck(paras, 2)
)


### Notes on function specification

# 1-3 use-case scenarios for the generic code
# don't want a specific case just for Scheelbeek
# as the model should be capable of application to different policies / subgroups etc

# use-case 1 - use evidence from Scheelbeek to estimate the effects of a 20% increase in high sugar snacks on costs and QALYs

# taking account of 3 subgroups of BMI < 25, 25-30, 30+
# (note that being able to apply different scaling factors to different values of BMI will require changes to the structure of the model
# in terms of how BMI is modelled or how the scaling factors are applied to BMI)

# also differentiate effects by income quintile

# As an example, could potentially take the effects on high sugar snacks from Scheelbeek

# The function needs to show how BMI would change based on the Sheelbeek parameters
# Suppose a 1 year effect on weight

# Question - how to stratify effects by starting BMI? - discuss options
# e.g. function that gave a change in BMI for age, sex, IMD quintile and ethnicity
# usually the bigger they are the harder they fall in BMI
# estimated effect from Sheelbeek paper corresponds to effect in average BMI person
# potential to introduce slope so weight change is proportional to BMI

# Question - what dimensions will the input data to the function have?

# Question - how to map between categories of interest in the input data
# and categories of interest in the model outcome variables

# Is income already in the SPHERE model? - think yes

# Aim for a prototype function that works with BMI categories and income - 
# this is difficult to do in the first version of the prototype function without understanding more

# Some people / policies will need to build the table manually
# In 10 years time, there is x effect - function can specify phased effect then constant effect
# If it only effects ages between 18 and 95, then have that as an option
# Adding nuances - function can enable quick building of assumptions 

### Function prototyping


# Imagine a potential structure for the input data to the function
# this would be the sort of parameters that are extracted from a paper to indicate policy effects

# For now just mock-up a potential structure

# To keep simple imagine only age and sex subgroups
# with a relative effect estimate corresponding to each subgroup combination

input_data <- data.frame(expand.grid(
  Sex = c("Male", "Female"),
  Age = c("18-24", "25-50", "50-90")
))
input_data$effect <- 1 + runif(nrow(input_data))


# Prototype the function

#' Function to generate scaling parameters
#'
#' @param input_data data.frame - the supposed input parameters from previous statistical analysis or assumptions
#' @param year_lim vector - the min and max years of the model simulation
#' @param year_scale vector - values between 0 and 1 indicating the degree of penetrance of the effect in each year
#' e.g. 1 = full effect in year 1, and could be set to have a gradually diminishing effect in subsequent years
#' @param parameter_label indicating the parameter that the scaling factor corresponds to e.g. "bmi_mean"
#' @param scenario_label indicating the scenario 
#'
#' @return Returns a table of scaling factors in the same form as the table currently input into the SPHERE model from the data lake
#' 
#' 
#' @export
#'
#' @examples
#' 
#' input_data <- data.frame(expand.grid(
#'   Sex = c("Male", "Female"),
#'   Age = c("18-24", "25-50", "50-90")
#'   ))
#' input_data$effect <- 1 + runif(nrow(input_data))
#' 
#' ScalingTable <- MakeScaling(
#'   input_data,
#'   year_lim = c(2010, 2020),
#'   year_scale = seq(1, 0, length.out = length(2010:2020)),
#'   parameter_label = "bmi_mean",
#'   scenario_label = "function test"
#' )
#' 
#' 
MakeScaling <- function(
  input_data,
  year_lim,
  year_scale,
  parameter_label,
  scenario_label
) {
  
  # Create a vector of years
  years <- year_lim[1]:year_lim[2]
  
  # Combine years and the year scaling factors
  year_scale_data <- data.frame(
    Year = years,
    year_scale
  )
  
  # Create the structure of the output table
  domain <- data.frame(expand.grid(
    Sex = unique(input_data$Sex),
    Age = unique(input_data$Age),
    Year = years
  ))
  domain$Scenario <- scenario_label
  domain$Parameter <- parameter_label
  
  # Add in the year scaling factors and the input parameters
  domain <- merge(domain, year_scale_data, by = "Year")
  domain <- merge(domain, input_data, by = c("Age", "Sex"))
  
  # Apply the year scaling factors
  domain$Scaling <- 1 + (domain$year_scale * (domain$effect - 1))
  
  # Delete columns not needed
  domain$year_scale <- NULL
  domain$effect <- NULL
  
  # Sort by year
  domain <- domain[order(domain$Year),]
  
  
  return(domain)
}






